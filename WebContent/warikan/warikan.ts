var totalElm: HTMLInputElement = <HTMLInputElement>document.getElementById("total");
var numElm: HTMLInputElement = <HTMLInputElement>document.getElementById("num");
//var unitElm: HTMLInputElement = <HTMLInputElement>document.getElementById("unit");
var resultElm: HTMLElement = document.getElementById("result");

window.onload = () => {
    totalElm.addEventListener("blur", () => { calcurate() });
    numElm.addEventListener("blur", () => { calcurate() });
//    unitElm.addEventListener("blur", () => { calcurate() });
};

var calcurate = () => {
    var total = parseInt(totalElm.value);
    var num = parseInt(numElm.value);
//   var unit = parseInt(unitElm.value);
    if (!isNaN(total) && !isNaN(num)) {
        var quotient: number = Math.floor(total / num);
        var remainder: number = total % num;
//        var organizerPay: number = total - (unit * num);
        resultElm.innerHTML = "一人あたり" + quotient + "円です。端数は" + remainder + "円です。<br>";
    } else {
        resultElm.innerHTML = "";
    }
};