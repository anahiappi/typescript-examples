window.onload = function () {
    marubatsu = ["〇", "×"];
    turn = 0;
    //3×3の各tdタグにイベントハンドラを割り当てる
    cell = new Array();
    for (var y = 0; y < 3; y++) {
        cell[y] = new Array();
        for (var x = 0; x < 3; x++) {
            cell[y][x] = document.getElementById("c" + (y + 1) + (x + 1));
            cell[y][x].onclick = changeSymbol(x, y); //クリックしたときのcell[y][x]セルに記号をつける
        }
    }
};
var changeSymbol = function (x, y) {
    return function (ev) {
        cell[y][x].innerHTML = marubatsu[turn % 2]; //クリック時、〇と×とを交互に出力する
        setTimeout(judgeWinner(), 0);
        //judgeWinner();
        turn++;
    };
};
var judgeWinner = function () {
    var result = "";
    //勝利判定フラグ
    var flag;
    //〇と×それぞれの勝敗判定
    for (var i = 0; i < 2; i++) {
        flag = false;
        //横勝利判定
        for (var y = 0; y < 3; y++) {
            if (cell[y][0].innerHTML === marubatsu[i] && cell[y][1].innerHTML === marubatsu[i] && cell[y][2].innerHTML === marubatsu[i]) {
                flag = true;
            }
        }
        //縦勝利判定
        for (var x = 0; x < 3; x++) {
            if (cell[0][x].innerHTML === marubatsu[i] && cell[1][x].innerHTML === marubatsu[i] && cell[2][x].innerHTML === marubatsu[i]) {
                flag = true;
            }
        }
        //斜め勝利判定
        if (cell[0][0].innerHTML === marubatsu[i] && cell[1][1].innerHTML === marubatsu[i] && cell[2][2].innerHTML === marubatsu[i]) {
            flag = true;
        }
        if (cell[0][2].innerHTML === marubatsu[i] && cell[1][1].innerHTML === marubatsu[i] && cell[2][0].innerHTML === marubatsu[i]) {
            flag = true;
        }
        //結果メッセージの生成
        result += flag ? (marubatsu[i] + "の勝ち") : "";
    }
    //結果メッセージを出力
    document.getElementById("winner").innerHTML = result;
};
//# sourceMappingURL=marubatsu.js.map